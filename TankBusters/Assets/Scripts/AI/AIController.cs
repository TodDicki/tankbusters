﻿using UnityEngine;
using System.Collections.Generic;

public class AIController : MonoBehaviour {

    public enum AIType {
        Default,
        Runner,
        Sentinel
    };

    public enum StateType
    {
        Patrol,
        Chase,
        Flee,
        Guard
    }

    [SerializeField]
    private AIType aiType = AIType.Default;//the ai behaviour type
    [SerializeField]
    private float detectionDistance = 5.0f;//how far the AI can see
    [SerializeField]
    private float fleeDistance = 5.0f;//how far the AI should flee while in the flee state

    
    [SerializeField]
    private Transform waypoint = null;
    [SerializeField]
    private Transform[] availableWaypoints;

    //Components
    private Transform tf = null;//this objects transform
    private Mover mover = null;//this objects mover component
    private Shooter shooter = null;//this objects shooter component

    //Path values
    private PathFinder pathFinder = null;
    private Vector3[] path;
    private bool pathSuccess = false;

    //finite state machine and states
    private FiniteStateMachine<AIController> fsm = new FiniteStateMachine<AIController>();
    private Dictionary<StateType, State<AIController>> availableStates = new Dictionary<StateType, State<AIController>>()
    {
        {StateType.Patrol, new AIPatrolState() },
        {StateType.Chase, new AIChaseState() },
        {StateType.Flee, new AIFleeState() },
        {StateType.Guard, new AIGuardState() }
    };

    //target
    private Transform target = null;

    public Vector3[] Path
    {
        get { return path; }
        set { path = value; }
    }

    public bool PathSuccess
    {
        get { return pathSuccess; }
        set { pathSuccess = value; }
    }

    public AIType AItype
    {
        get { return aiType; }
    }

    public Dictionary<StateType, State<AIController>> AvailableStates
    {
        get { return availableStates; }
    }

    public float FleeDistance
    {
        get { return fleeDistance; }
    }

    public Transform Tf
    {
        get { return tf; }
    }

    public Transform Target
    {
        get { return target; }
    }

    public Mover Mover
    {
        get { return mover; }
    }

    public Shooter Shooter
    {
        get { return shooter; }
    }
    public Transform CurrentWaypoint
    {
        get { return waypoint; }
    }

    public Transform[] AvailableWaypoints
    {
        get { return availableWaypoints; }
        set { availableWaypoints = value; }
    }

    public PathFinder PathFinder
    {
        get { return pathFinder; }
    }

    public FiniteStateMachine<AIController> FSM
    {
        get { return fsm; }
    }
	



	// Use this for initialization
	void Start () {
        //Get components
        tf = GetComponent<Transform>();
        shooter = GetComponent<Shooter>();
        mover = GetComponent<Mover>();

        //get the pathfinder
        pathFinder = FindObjectOfType<PathFinder>();

        //initialize the state machine
        fsm.Initialize(this);

        //set starting state
        if (aiType == AIType.Sentinel)
            fsm.ChangeState(availableStates[StateType.Guard]);
        else
            fsm.ChangeState(availableStates[StateType.Patrol]);

        //start the state machine
        fsm.Start();
    }

    /// <summary>
    /// Find a random waypoint.
    /// </summary>
    public void RandomWaypoint()
    {
        waypoint = availableWaypoints[Random.Range(0, availableWaypoints.Length - 1)];
    }

    /// <summary>
    /// Checks to see if a player is within the range.
    /// </summary>
    public void SearchForTarget()
    {
        Collider[] potentialTargets = Physics.OverlapSphere(tf.position, detectionDistance);
        List<Transform> targets = new List<Transform>(); ;
        for (int i = 0; i < potentialTargets.Length; i++)
        {
            if (potentialTargets[i].GetComponent<PlayerController>() != null)
                targets.Add(potentialTargets[i].transform);
        }

        target = GetClosestObjectFromList(targets);
    }

    /// <summary>
    /// Returns the closest transform in a list of transforms.
    /// </summary>
    /// <param name="listToSearch"></param>
    /// <returns></returns>
    private Transform GetClosestObjectFromList(List<Transform> listToSearch)
    {
        float currentDist = Mathf.Infinity;
        Transform closest = null;

        for (int i = 0; i < listToSearch.Count; i++)
        {
            Transform currentObj = listToSearch[i];


            if (currentObj != null && currentObj != tf)
            {
                float dist = Mathf.Abs((tf.position - currentObj.position).sqrMagnitude);
                if (dist < currentDist)
                {
                    currentDist = dist;
                    closest = currentObj;
                }
            }
        }

        return closest;
    }

    /// <summary>
    /// This function is called when a PathRequest is processed by the pathfinder.
    /// </summary>
    /// <param name="waypoints"></param>
    /// <param name="pathSuccessful"></param>
    public void OnPathFound(Vector3[] waypoints, bool pathSuccessful)
    {
        if (pathSuccessful)
        {
            path = waypoints;
        }
        pathSuccess = pathSuccessful;
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.black;
        for (int i = 0; i < path.Length; i++)
        {
            Gizmos.DrawWireSphere(path[i], 1);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        for (int i = 1; i < path.Length; i++)
        {           
            Gizmos.DrawLine(path[i - 1], path[i]);   
        }
    }
#endif
}

public class AIPatrolState : State<AIController>
{
    int pathIndex = 0;

    public override void OnEntry(AIController obj)
    {
        //request a path to a random waypoint
        obj.PathSuccess = false;
        pathIndex = 0;
        obj.RandomWaypoint();
        obj.PathFinder.RequestPath(new PathRequest(obj.Tf.position, obj.CurrentWaypoint.position, obj.OnPathFound));
    }

    

    public override void OnExit(AIController obj)
    {
    }

    public override void Run(AIController obj)
    {
        //check for a target
        obj.SearchForTarget();

        //if there is a target
        if (obj.Target != null)
        {
            //switch to the appropriate state
            if (obj.AItype == AIController.AIType.Runner)
                obj.FSM.ChangeState(obj.AvailableStates[AIController.StateType.Flee]);
            else
                obj.FSM.ChangeState(obj.AvailableStates[AIController.StateType.Chase]);

            return;
        }

        //if the path hasn't been found yet
        if (!obj.PathSuccess)
            return;//wait


        //pathfind to waypoint
        Vector3 currentPoint = obj.Path[pathIndex];
        currentPoint.y = obj.Tf.position.y;
        obj.Mover.RotateTowards(currentPoint);
        obj.Mover.Move(obj.Tf.forward);
        float dist = (obj.Tf.position - currentPoint).sqrMagnitude;
        if (dist < 1)
        {
            pathIndex++;
            if (pathIndex >= obj.Path.Length)//if the end of the path has been reached
            {
                OnEntry(obj);//find a new path
            }
        }
    }
}

public class AIChaseState : State<AIController>
{

    public override void OnEntry(AIController obj)
    {
    }

    public override void OnExit(AIController obj)
    {
    }

    public override void Run(AIController obj)
    {
        //check for a target
        obj.SearchForTarget();

        //if there is no target
        if (obj.Target == null)
        {
            //change to the patrol state
            obj.FSM.ChangeState(obj.AvailableStates[AIController.StateType.Patrol]);
            return;
        }

        //requst a path to the target
        obj.PathFinder.RequestPath(new PathRequest(obj.Tf.position, obj.Target.position, obj.OnPathFound));

        if (!obj.PathSuccess)
            return;

        //move to the target
        Vector3 targetPos = obj.Path[0];
        targetPos.y = obj.Tf.position.y;
        obj.Mover.RotateTowards(targetPos);
        obj.Mover.Move(targetPos);

        //shoot at the target
        obj.Shooter.Fire();
    }
}

public class AIFleeState : State<AIController>
{
    int pathIndex = 0;

    public override void OnEntry(AIController obj)
    {
        //pick a point away from the target
        Vector3 vecToTarget = obj.Target.position - obj.Tf.position;
        vecToTarget.Normalize();
        vecToTarget *= -obj.FleeDistance;

        //request a path
        obj.PathSuccess = false;
        obj.PathFinder.RequestPath(new PathRequest(obj.Tf.position, obj.Tf.position + vecToTarget, obj.OnPathFound));
    }


    public override void OnExit(AIController obj)
    {
    }

    public override void Run(AIController obj)
    {
        //pathfind to waypoint
        if (!obj.PathSuccess)
            return;

        Vector3 currentPoint = obj.Path[pathIndex];
        currentPoint.y = obj.Tf.position.y;
        obj.Mover.RotateTowards(currentPoint);
        obj.Mover.Move(obj.Tf.forward);

        float dist = (obj.Tf.position - currentPoint).sqrMagnitude;
        if (dist < 1)
        {
            pathIndex++;
            if (pathIndex >= obj.Path.Length)
            {
                obj.FSM.ChangeState(obj.AvailableStates[AIController.StateType.Patrol]);
                return;
            }
        }
    }
}

public class AIGuardState : State<AIController>
{
    public override void OnEntry(AIController obj)
    {
    }

    public override void OnExit(AIController obj)
    {
    }

    public override void Run(AIController obj)
    {
        //search for a target
        obj.SearchForTarget();

        //if there is a target
        if (obj.Target != null)
        {
            //aim at the target and shoot
            obj.Mover.RotateTowards(obj.Target.position);
            obj.Shooter.Fire();
        }
    }
}