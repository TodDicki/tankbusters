﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiniteStateMachine<T> where T : MonoBehaviour
{
    private State<T> currentState;
    private bool isRunning = false;
    private T obj = null;

    public void ChangeState(State<T> newState)
    {
        if(currentState != null)
            currentState.OnExit(obj);
        currentState = newState;
        currentState.OnEntry(obj);
    }

    public void Initialize(T obj)
    {
        this.obj = obj;
    }

    public void Start()
    {
        isRunning = true;
        obj.StartCoroutine(Run());
    }


    public void Stop()
    {
        isRunning = false;
    }

    IEnumerator Run()
    {
        while (isRunning)
        {
            currentState.Run(obj);
            yield return null;
        }
    }

}

public abstract class State<T> where T : MonoBehaviour
{
    public abstract void OnEntry(T obj);
    public abstract void Run(T obj);
    public abstract void OnExit(T obj);
}
