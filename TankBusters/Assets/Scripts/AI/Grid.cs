﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid {

    private int nodeAmountX = 0;
    private int nodeAmountY = 0;

    public float nodeUnitSize = 0;

    private Transform initialPosition = null;

    private LayerMask unwalkableLayerMask;

    private Node[,] grid = null;

    private HashSet<Node> unwalkableNodes;

    Vector3 bottomLeft = Vector3.zero;

    public int NodeAmountX
    {
        get { return nodeAmountX; }
    }

    public int NodeAmountY
    {
        get { return nodeAmountY; }
    }

    public Node[,] WorldGrid
    {
        get { return grid; }
    }

    public HashSet<Node> UnwalkableNodes
    {
        get { return unwalkableNodes; }
    }

    public int GridSize
    {
        get { return nodeAmountX * nodeAmountY; }
    }

    /// <summary>
    /// Initialize the grid.
    /// </summary>
    /// <param name="initPosition"></param>
    /// <param name="unwalkableLayerMask"></param>
    /// <param name="nodeUnitSize"></param>
    /// <param name="nodeAmountX"></param>
    /// <param name="nodeAmountY"></param>
    public void InitializeGrid(Transform initPosition, LayerMask unwalkableLayerMask, float nodeUnitSize, int nodeAmountX, int nodeAmountY)
    {
        initialPosition = initPosition;
        this.unwalkableLayerMask = unwalkableLayerMask;
        this.nodeUnitSize = nodeUnitSize;
        this.nodeAmountX = nodeAmountX;
        this.nodeAmountY = nodeAmountY;

        unwalkableNodes = new HashSet<Node>();
        grid = new Node[nodeAmountX, nodeAmountY];

        CreateGrid();
    }

    /// <summary>
    /// Create the grid of nodes.
    /// </summary>
    private void CreateGrid()
    {
        //calculate the bottom left corner of the grid
        Vector3 xOffset = Vector3.right * ((initialPosition.localScale.x) / 2);
        Vector3 yOffset = Vector3.forward * ((initialPosition.localScale.z) / 2);
        bottomLeft = initialPosition.position - xOffset - yOffset;

        for (int x = 0; x < nodeAmountX; x++)
        {
            for (int y = 0; y < nodeAmountY; y++)
            {
                //calculate the world position of the node
                float xCalc = x * nodeUnitSize;
                float yCalc = y * nodeUnitSize;
                Vector3 worldPoint = bottomLeft + (Vector3.right * xCalc) + (Vector3.forward * yCalc);

                //determine if the node is walkable
                bool walkable = !(Physics.OverlapSphere(worldPoint, nodeUnitSize, unwalkableLayerMask).Length > 0);

                //create the node
                grid[x, y] = new Node(worldPoint, x, y, nodeUnitSize);

                //if the node is not walkable, add to the unwalkable set
                if (!walkable)
                    unwalkableNodes.Add(grid[x, y]);
            }
        }
    }

    /// <summary>
    /// Updates the status of unwalkable nodes.
    /// Not really useful for this project,
    /// but can be used for dynamic environments.
    /// </summary>
    public void UpdateWalkable()
    {
        //clear the unwalkable nodes
        unwalkableNodes.Clear();

        for (int x = 0; x < nodeAmountX; x++)
        {
            for (int y = 0; y < nodeAmountY; y++)
            {
                float xCalc = x * nodeUnitSize;
                float yCalc = y * nodeUnitSize;
                Vector3 worldPoint = bottomLeft + (Vector3.right * xCalc) + (Vector3.forward * yCalc);

                //determine if the node is walkable
                bool walkable = !(Physics.OverlapSphere(worldPoint, nodeUnitSize, unwalkableLayerMask).Length > 0);

                //if the node is not walkable, add to the unwalkable set
                if (!walkable)
                    unwalkableNodes.Add(grid[x, y]);
            }
        }
    }

    /// <summary>
    /// Grabs the nearest node to a world position
    /// </summary>
    /// <param name="nodeX"></param>
    /// <param name="nodeY"></param>
    /// <returns></returns>
    public Node GetNodeFromPoint(int nodeX, int nodeY)
    {
        float percentX = nodeX / (float)nodeAmountX;
        float percentY = nodeY / (float)nodeAmountY;

        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x = Mathf.RoundToInt((nodeAmountX / nodeUnitSize) * percentX);
        int y = Mathf.RoundToInt((nodeAmountY / nodeUnitSize) * percentY);

        x = Mathf.Clamp(x, 0, nodeAmountX - 1);
        y = Mathf.Clamp(y, 0, nodeAmountY - 1);

        return grid[x, y];
    }

    /// <summary>
    /// Grabs the node at the index.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public Node GetNodeFromIndex(int x, int y)
    {
        if (!IsWalkable(x, y))
            return null;
        return grid[x, y];
    }

    /// <summary>
    /// Finds the neighbours of a node and does the neccessary pruning for Jump Point search.
    /// </summary>
    /// <param name="currentNode"></param>
    /// <returns></returns>
    public List<Node> GetNeighbours(Node currentNode)
    {
        List<Node> neighbors = new List<Node>();
        Node parentNode = currentNode.parent;

        if (parentNode != null)
        {
            int dx = Mathf.Clamp((currentNode.x - parentNode.x), -1, 1);
            int dy = Mathf.Clamp((currentNode.y - parentNode.y), -1, 1);

            //diagonal pruning
            if (dx != 0 && dy != 0)
            {
                bool cmp1 = IsWalkable(currentNode.x, currentNode.y + dy);
                bool cmp2 = IsWalkable(currentNode.x + dx, currentNode.y);
                bool cmp3 = IsWalkable(currentNode.x - dx, currentNode.y);
                bool cmp4 = IsWalkable(currentNode.x, currentNode.y - dy);

                if (cmp1)
                    neighbors.Add(GetNodeFromIndex(currentNode.x, currentNode.y + dy));
                if (cmp2)
                    neighbors.Add(GetNodeFromIndex(currentNode.x + dx, currentNode.y));
                if (cmp1 || cmp2)
                    neighbors.Add(GetNodeFromIndex(currentNode.x + dx, currentNode.y + dy));
                if (!cmp3 && cmp1)
                    neighbors.Add(GetNodeFromIndex(currentNode.x - dx, currentNode.y + dy));
                if (!cmp4 && cmp2)
                    neighbors.Add(GetNodeFromIndex(currentNode.x + dx, currentNode.y - dy));
            }
            else
            {
                if (dx == 0)
                {
                    if (IsWalkable(currentNode.x, currentNode.y + dy))
                    {
                        neighbors.Add(GetNodeFromIndex(currentNode.x, currentNode.y + dy));
                        if (!IsWalkable(currentNode.x + 1, currentNode.y))
                            neighbors.Add(GetNodeFromIndex(currentNode.x + 1, currentNode.y + dy));
                        if (!IsWalkable(currentNode.x - 1, currentNode.y))
                            neighbors.Add(GetNodeFromIndex(currentNode.x - 1, currentNode.y + dy));
                    }
                }
                else
                {
                    if (IsWalkable(currentNode.x + dx, currentNode.y))
                    {
                        neighbors.Add(GetNodeFromIndex(currentNode.x + dx, currentNode.y));
                        if (!IsWalkable(currentNode.x, currentNode.y + 1))
                            neighbors.Add(GetNodeFromIndex(currentNode.x + dx, currentNode.y + 1));
                        if (!IsWalkable(currentNode.x, currentNode.y - 1))
                            neighbors.Add(GetNodeFromIndex(currentNode.x + dx, currentNode.y - 1));
                    }
                }
            }
        }
        else
        {
            if (IsWalkable(currentNode.x + 0, currentNode.y + 1))
                neighbors.Add(GetNodeFromIndex(currentNode.x + 0, currentNode.y + 1));
            if (IsWalkable(currentNode.x + 0, currentNode.y - 1))
                neighbors.Add(GetNodeFromIndex(currentNode.x + 0, currentNode.y - 1));
            if (IsWalkable(currentNode.x + 1, currentNode.y + 0))
                neighbors.Add(GetNodeFromIndex(currentNode.x + 1, currentNode.y + 0));
            if (IsWalkable(currentNode.x - 1, currentNode.y + 0))
                neighbors.Add(GetNodeFromIndex(currentNode.x - 1, currentNode.y + 0));

            if (IsWalkable(currentNode.x + 1, currentNode.y + 1))
                neighbors.Add(GetNodeFromIndex(currentNode.x + 1, currentNode.y + 1));
            if (IsWalkable(currentNode.x + 1, currentNode.y - 1))
                neighbors.Add(GetNodeFromIndex(currentNode.x + 1, currentNode.y - 1));
            if (IsWalkable(currentNode.x - 1, currentNode.y + 1))
                neighbors.Add(GetNodeFromIndex(currentNode.x - 1, currentNode.y + 1));
            if (IsWalkable(currentNode.x - 1, currentNode.y - 1))
                neighbors.Add(GetNodeFromIndex(currentNode.x - 1, currentNode.y - 1));
        }

        return neighbors;

    }

    /// <summary>
    /// Returns true if the node is in the grid and is not in the unwalkable nodes set.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public bool IsWalkable(int x, int y)
    {
        return (x >= 0 && x < nodeAmountX) && (y >= 0 && y < nodeAmountY) && !unwalkableNodes.Contains(grid[x, y]);
    }

}
