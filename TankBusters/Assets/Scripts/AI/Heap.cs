﻿using System;
using System.Collections;

public class Heap<T> where T : IHeapItem<T>
{
    private T[] items = null;
    private int currentItemCount = 0;

    public int Count
    {
        get
        {
            return currentItemCount;
        }
    }

    public Heap(int maxHeapSize)
    {
        items = new T[maxHeapSize];
    }

    public T[] ToArray()
    {
        return items;
    }

    /// <summary>
    /// Add an item to the heap.
    /// </summary>
    /// <param name="item"></param>
    public void Add(T item)
    {
        item.HeapIndex = currentItemCount;
        items[currentItemCount] = item;
        SortUp(item);
        currentItemCount++;
    }

    /// <summary>
    /// Returns the next item in the heap.
    /// </summary>
    /// <returns></returns>
    public T GetNext()
    {
        T firstItem = items[0];
        currentItemCount--;
        items[0] = items[currentItemCount];
        items[0].HeapIndex = 0;
        SortDown(items[0]);
        return firstItem;
    }

    public void UpdateItem(T item)
    {
        SortUp(item);
    }

    public bool Contains(T item)
    {
        return Equals(items[item.HeapIndex], item);
    }

    private void SortUp(T item)
    {
        int parentIndex = (item.HeapIndex - 1) / 2;

        while (true)
        {
            T parentItem = items[parentIndex];
            if (item.CompareTo(parentItem) > 0)
            {
                Swap(item, parentItem);
            }
            else
                break;

            parentIndex = (item.HeapIndex - 1) / 2;
        }
    }

    private void SortDown(T item)
    {
        while (true)
        {
            int childLeftIndex = (item.HeapIndex * 2) + 1;
            int childRightIndex = (item.HeapIndex * 2) + 2;
            int swapIndex = 0;

            if (childLeftIndex < currentItemCount)
            {
                swapIndex = childLeftIndex;
                if (childRightIndex < currentItemCount)
                {
                    if (items[childLeftIndex].CompareTo(items[childRightIndex]) < 0)
                    {
                        swapIndex = childRightIndex;
                    }
                }

                if (item.CompareTo(items[swapIndex]) < 0)
                {
                    Swap(item, items[swapIndex]);
                }
                else
                    return;
            }
            else
                return;

        }
    }

    private void Swap(T itemA, T itemB)
    {
        items[itemA.HeapIndex] = itemB;
        items[itemB.HeapIndex] = itemA;

        int tempIndex = itemA.HeapIndex;
        itemA.HeapIndex = itemB.HeapIndex;
        itemB.HeapIndex = tempIndex;
    }

    public void Clear()
    {
        Array.Clear(items, 0, currentItemCount);
        currentItemCount = 0;
    }

}

public interface IHeapItem<T> : IComparable<T>
{
    int HeapIndex
    {
        get;
        set;
    }
}