﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : IHeapItem<Node> {

    public Node parent;
    public Vector3 worldPosition;
    public float nodeSize;
    public int x;
    public int y;
    public bool forced = false;

    private int heapIndex = 0;
    public int HeapIndex
    {
        get { return heapIndex; }
        set { heapIndex = value; }
    }

    public int gCost;
    public int hCost;
    public int fCost
    {
        get { return gCost + hCost; }
    }

    public Node(Vector3 worldPoint, int _x, int _y, float _nodeSize)
    {
        worldPosition = worldPoint;
        nodeSize = _nodeSize;
        x = _x;
        y = _y;
    }

    public int CompareTo(Node nodeToCompare)
    {
        int comparison = fCost.CompareTo(nodeToCompare.fCost);
        if (comparison == 0)
            comparison = hCost.CompareTo(nodeToCompare.hCost);

        return -comparison;
    }

    public override string ToString()
    {
        return x.ToString() + ", " + y.ToString();
    }
}
