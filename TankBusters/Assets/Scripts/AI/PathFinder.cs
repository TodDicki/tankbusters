﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using System;
using System.Collections.ObjectModel;
using System.Threading;

[Serializable]
public struct PathRequest
{
    public Vector3 start, end;
    public Action<Vector3[], bool> callback;

    public PathRequest(Vector3 startPos, Vector3 endPos, Action<Vector3[], bool> aCallback)
    {
        start = startPos;
        end = endPos;
        callback = aCallback;
    }
}

public struct PathResult
{
    public Vector3[] path;
    public bool success;
    public Action<Vector3[], bool> callback;

    public PathResult(Vector3[] aPath, bool aSuccess, Action<Vector3[], bool> aCallback)
    {
        path = aPath;
        success = aSuccess;
        callback = aCallback;
    }
}

public class PathFinder : MonoBehaviour {

    [SerializeField]
    private int pathProcesses = 10;

    [SerializeField]
    private bool dynamicGrid = false;
    [SerializeField]
    private float gridUpdateTimer = 10;


    [SerializeField]
    private int nodeAmountXperTile = 500;
    [SerializeField]
    private int nodeAmountYperTile = 500;

    [SerializeField]
    private float nodeUnitSize = 1;

    [SerializeField]
    private LayerMask unwalkableLayerMask;
#if UNITY_EDITOR
    [SerializeField]
    private bool showDebug = true;
#endif

    private bool initialized = false;


    private List<Node> jumpNodes;

    private Node startNode;
    private Node targetNode;

    private Grid grid;

    private Heap<Node> openSet;
    private HashSet<Node> openSetContainer;
    private HashSet<Node> closedSet;
    private bool forced;

    private bool processingPaths = false;

    private Queue<PathRequest> requests = new Queue<PathRequest>();

#if UNITY_EDITOR
    //Used for Debugging
    private float totalPathTime = 0;
    private int totalPaths = 0;
#endif

    public ReadOnlyCollection<Node> JumpNodes
    {
        get { return jumpNodes.AsReadOnly(); }
    }

    public HashSet<Node> ClosedSet
    {
        get { return closedSet; }
    }


    public void RequestPath(PathRequest pr)
    {
        requests.Enqueue(pr);
        if (!processingPaths)
            StartCoroutine(ProcessPaths());
    }

    IEnumerator UpdateGridTimer()
    {
        yield return new WaitForSeconds(gridUpdateTimer);
        grid.UpdateWalkable();
        StartCoroutine(UpdateGridTimer());
    }

    IEnumerator ProcessPaths()
    {
        processingPaths = true;
        while (requests.Count > 0)
        {
            int iterations = requests.Count;
            if (iterations > pathProcesses)
                iterations = pathProcesses;

            for (int i = 0; i < iterations; i++)
            {
                PathRequest request = requests.Dequeue();
                PathResult result = FindPath(request);
                result.callback(result.path, result.success);
            }
            yield return null;
        }
        processingPaths = false;
#if UNITY_EDITOR
        UnityEngine.Debug.Log(totalPaths + " paths found. Average time: " + totalPathTime / totalPaths + " ms");
#endif
    }

    public PathResult FindPath(PathRequest request)
    {
        if (!initialized)
            Initialize();

        bool pathSuccess;

        Node start = grid.GetNodeFromPoint((int)request.start.x, (int)request.start.z);
        Node end = grid.GetNodeFromPoint((int)request.end.x, (int)request.end.z);

        Vector3[] ret = GetPath(start, end, out pathSuccess);

        return new PathResult(ret, pathSuccess, request.callback);
    }

    Vector3[] GetPath(Node start, Node end, out bool success)
    {
        startNode = start;
        targetNode = end;

        ResetValues();


#if UNITY_EDITOR
        Stopwatch sw = new Stopwatch();
        sw.Start();
#endif

        success = CalculateShortestPath();

#if UNITY_EDITOR
        sw.Stop();
        TimeSpan ts = sw.Elapsed;
        totalPathTime += ts.Milliseconds;
        totalPaths++;
        UnityEngine.Debug.Log((success ? "Path found in : " : "No path found in : ") + ts.Milliseconds + " ms");
#endif

        Vector3[] ret = new Vector3[0];
        if (success)
        {
            List<Node> path = RetracePath();
            ret = new Vector3[path.Count];
            for (int i = 0; i < path.Count; i++)
            {
                ret[i] = path[i].worldPosition;
            }
            success = ret.Length > 0;
        }
        return ret;
    }



    private void Initialize()
    {
        PersistentData data = FindObjectOfType<PersistentData>();
        grid = new Grid();
        grid.InitializeGrid(transform, unwalkableLayerMask, nodeUnitSize, nodeAmountXperTile * data.MapWidth, nodeAmountYperTile * data.MapHeight);
        openSet = new Heap<Node>(grid.GridSize);
        openSetContainer = new HashSet<Node>();
        closedSet = new HashSet<Node>();
        jumpNodes = new List<Node>();
        if (dynamicGrid)
            StartCoroutine(UpdateGridTimer());

        initialized = true;
        
    }

    private void ResetValues()
    {
        openSet.Clear();
        openSetContainer.Clear();
        closedSet.Clear();
        jumpNodes.Clear();
    }

    private bool CalculateShortestPath()
    {
        Node currentNode;

        openSet.Add(startNode);
        openSetContainer.Add(startNode);

        while (openSet.Count > 0)
        {
            currentNode = openSet.GetNext();
            openSetContainer.Remove(currentNode);

            if (currentNode == targetNode)
            {
                return true;
            }
            else
            {
                closedSet.Add(currentNode);
                List<Node> nodes = GetSuccessors(currentNode);

                foreach (Node node in nodes)
                {
                    jumpNodes.Add(node);

                    if (closedSet.Contains(node))
                        continue;

                    int newGCost = currentNode.gCost + GetDistance(currentNode, node);
                    if (newGCost < node.gCost || !openSetContainer.Contains(node))
                    {
                        node.gCost = newGCost;
                        node.hCost = GetDistance(node, targetNode);
                        node.parent = currentNode;

                        if (!openSetContainer.Contains(node))
                        {
                            openSetContainer.Add(node);
                            openSet.Add(node);
                        }
                        else
                        {
                            openSet.UpdateItem(node);
                        }

                    }

                }
            }
        }

        return false;
    }

    private List<Node> RetracePath()
    {
        List<Node> path = new List<Node>();
        Node currentNode = targetNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }

        path.Reverse();

        return path;
    }

    private List<Node> GetSuccessors(Node currentNode)
    {
        Node jumpNode;

        List<Node> successors = new List<Node>();
        List<Node> neighbours = grid.GetNeighbours(currentNode);

        foreach (Node neighbour in neighbours)
        {
            if(neighbour != null)
            {
                int xDirection = neighbour.x - currentNode.x;
                int yDirection = neighbour.y - currentNode.y;

                jumpNode = Jump(currentNode, xDirection, yDirection);

                if (jumpNode != null)
                    successors.Add(jumpNode);
            }          
        }
        return successors;
    }

    private Node Jump(Node initialNode, int xDirection, int yDirection)
    {
        Node n = grid.GetNodeFromIndex(initialNode.x + xDirection, initialNode.y + yDirection);
       
        if (n == null || !grid.IsWalkable(n.x, n.y))
        {
            return null;
        }
       
        if (n == targetNode)
        {
            return n;
        }

        if (xDirection != 0 && yDirection != 0)
        {
            if (!grid.IsWalkable(n.x - xDirection, n.y) && grid.IsWalkable(n.x - xDirection, n.y + yDirection) ||
                !grid.IsWalkable(n.x, n.y - yDirection) && grid.IsWalkable(n.x + xDirection, n.y - yDirection))
            {
                return n;
            }
        }
        else
        {
            if (xDirection != 0)
            {
                if (grid.IsWalkable(n.x + xDirection, n.y + 1) && !grid.IsWalkable(n.x, n.y + 1) ||
                    grid.IsWalkable(n.x + xDirection, n.y - 1) && !grid.IsWalkable(n.x, n.y - 1))
                {
                    return n;
                }
            }
            else
            {
                if (grid.IsWalkable(n.x + 1, n.y + yDirection) && !grid.IsWalkable(n.x + 1, n.y) ||
                    grid.IsWalkable(n.x -1, n.y +yDirection) && !grid.IsWalkable(n.x - 1, n.y))
                {
                    return n;
                }
            }
        }
        if (xDirection != 0 && yDirection != 0)
        {
            if (Jump(n, xDirection, 0) != null || Jump(n, 0, yDirection) != null)
            {
                return n;
            }
        }

        return Jump(n, xDirection, yDirection);
    }

    private int GetDistance(Node a, Node b)
    {
        int distX = Math.Abs(a.x - b.x);
        int distY = Mathf.Abs(a.y - b.y);

        return (distX > distY) ? 14 * distY + 10 * (distX - distY) : 14 * distX + 10 * (distY - distX);

    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        if (showDebug && grid != null)
        {
            Gizmos.color = Color.green;
            for (int x = 0; x < grid.NodeAmountX; x++)
            {
                for (int y = 0; y < grid.NodeAmountY; y++)
                {
                    if (x % 3 == 0 && y % 3 == 0)
                    {
                        Node n = grid.GetNodeFromIndex(x, y);
                        if (n != null)
                        {
                            Gizmos.DrawWireSphere(n.worldPosition, 1);
                        }
                    }
                }
            }
        }  
    }
#endif

}
