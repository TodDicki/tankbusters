﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    private PersistentData data = null;

    [SerializeField]
    private AudioSource ambientSource = null;

	// Use this for initialization
	void Start () {
        data = FindObjectOfType<PersistentData>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //I don't like having this in an Update, but I haven't set up an event structure for the Persistent Data that can handle multiple scenes
        if(data != null)
            ambientSource.volume = data.MusicVolume;	
	}

    public void PlayAudio(AudioClip clip, Vector3 position)
    {
        AudioSource.PlayClipAtPoint(clip, position, data.SFXVolume);
    }
}
