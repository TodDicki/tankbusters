﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Control Scheme")]
public class ControlScheme : ScriptableObject {
    public string verticalAxis = "Vertical";
    public string horizontalAxis = "Horizontal";
    public string fireInput = "Fire1";
}
