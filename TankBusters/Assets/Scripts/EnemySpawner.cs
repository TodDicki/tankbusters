﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour {


    [System.Serializable]
    struct SpawnWeight
    {
        public GameObject prefab;
        public float weight;
    }
    
    [SerializeField]
    private SpawnWeight[] enemyPrefabs;

    private Transform tf = null;//object that holds position for spawning enemies


    // Use this for initialization
    void Awake()
    {
        tf = GetComponent<Transform>();
    }

    public void SpawnEnemies(List<Waypoint> enemyWaypoints)
    {
        int enemiesToSpawn = FindObjectOfType<PersistentData>().DifficultyLevel;

        Transform[] waypoints = new Transform[enemyWaypoints.Count];
        for (int i = 0; i < enemyWaypoints.Count; i++)
        {
            waypoints[i] = enemyWaypoints[i].GetComponent<Transform>();
        }

        float totalWeight = 0;
        for (int i = 0; i < enemyPrefabs.Length; i++)
        {
            totalWeight += enemyPrefabs[i].weight;
        }

        for (int i = 0; i < enemiesToSpawn; i++)
        {
            float selectedWeight = Random.Range(0, totalWeight);
            float currentWeight = 0;

            for (int j = 0; j < enemyPrefabs.Length; j++)
            {
                if (currentWeight >= selectedWeight)
                {
                    Vector3 instantiationPoint = tf.position + (Random.onUnitSphere * 7);
                    instantiationPoint.y = 1f;
                    GameObject newEnemy = Instantiate(enemyPrefabs[j].prefab, instantiationPoint, Quaternion.identity);
                    newEnemy.GetComponent<AIController>().AvailableWaypoints = waypoints;
                    break;
                }
                currentWeight += enemyPrefabs[j].weight;
            }
        }
    }



}