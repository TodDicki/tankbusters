﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHeap : MonoBehaviour {

    [SerializeField]
    private GameObject item;
    [SerializeField]
    private int initialAmount = 50;

    private Queue<GameObject> items = new Queue<GameObject>(50);

    private Transform tf = null;

	// Use this for initialization
	void Awake () {
        tf = GetComponent<Transform>();
        SpawnSet();
	}

    void SpawnSet()
    {
        for (int i = 0; i < initialAmount; i++)
        {
            GameObject obj = Instantiate(item, tf.position, Quaternion.identity, tf);
            obj.SetActive(false);
            obj.GetComponent<ItemHeapItem>().Heap = this;
            items.Enqueue(obj);
        }
    }

    public GameObject GetNext()
    {
        if (items.Count <= 0)
            SpawnSet();

        GameObject obj = items.Dequeue();
        while (obj == null)
            obj = items.Dequeue();

        obj.SetActive(true);
        obj.transform.SetParent(null);
        obj.GetComponent<ItemHeapItem>().OnRetrieval.Invoke();
        return obj;
    }

    public void ReturnObject(GameObject obj)
    {
        obj.SetActive(false);
        obj.transform.SetParent(tf);
        obj.GetComponent<ItemHeapItem>().OnReturn.Invoke();

        items.Enqueue(obj);
        
    }
}

public interface ItemHeapItem
{
    ItemHeap Heap
    {
        set;
    }

    UnityEngine.Events.UnityEvent OnRetrieval
    {
        get;
    }

    UnityEngine.Events.UnityEvent OnReturn
    {
        get;
    }
}
