﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class MapGenerator : MonoBehaviour {

    [SerializeField]
    private Room[] tilePrefabs;//array of map segments to use

    private Room[,] grid;
    [SerializeField]
    private float tileWidth = 50;
    [SerializeField]
    private float tileLength = 50;

    PersistentData data = null;


    public float ColSize
    {
        get { return data.MapWidth * tileWidth; }
    }

    public float RowSize
    {
        get { return data.MapHeight * tileLength; }
    }

    Vector3 initialPosition = Vector3.zero;

    
	// Use this for initialization
	void Start () {
        data = FindObjectOfType<PersistentData>();

        DateTime currentDate = DateTime.Now;
        if (data.MapOfTheDay)
        {
            currentDate = currentDate.Date;
            UnityEngine.Random.InitState((int)currentDate.Ticks);
        }
        else
        {
            UnityEngine.Random.InitState(data.Seed);
        }


        GenerateMap();
	}

    /// <summary>
    /// Generates the map
    /// </summary>
    public void GenerateMap() {

        grid = new Room[data.MapWidth, data.MapHeight];

        List<Waypoint> allWaypoints = new List<Waypoint>();
        List<EnemySpawner> spawners = new List<EnemySpawner>();

        Vector3 colOffset = Vector3.right * (tileWidth);
        Vector3 rowOffset = Vector3.forward * (tileLength);
        Vector3 tileOffset = new Vector3(tileWidth / 2, 0, tileLength / 2);

        PlayerManager pm = FindObjectOfType<PlayerManager>();

        for (int currentColumn = 0; currentColumn < data.MapWidth; currentColumn++){//iterate through size x
            for (int currentRow = 0; currentRow < data.MapHeight; currentRow++){//iterate through size y

                Vector3 location = initialPosition + (colOffset * (float)currentColumn) + (rowOffset * (float)currentRow) + tileOffset;

                Room newTile = Instantiate(GetRandomTile(), location, Quaternion.identity);

                grid[currentColumn, currentRow] = newTile;

                allWaypoints.AddRange(newTile.GetComponentsInChildren<Waypoint>());
                spawners.AddRange(newTile.GetComponentsInChildren<EnemySpawner>());

                SpawnPoint[] playerSpawns = newTile.GetComponentsInChildren<SpawnPoint>();
                Transform[] spawnpointTransforms = new Transform[playerSpawns.Length];
                for (int i = 0; i < playerSpawns.Length; i++)
                {
                    spawnpointTransforms[i] = playerSpawns[i].GetComponent<Transform>();
                }

                
                pm.PlayerSpawns.AddRange(spawnpointTransforms);

                if (currentColumn > 0)
                {
                    grid[currentColumn, currentRow].WestDoor.SetActive(false);
                }
                if (currentColumn < data.MapWidth - 1)
                {
                    grid[currentColumn, currentRow].EastDoor.SetActive(false);
                }
                if (currentRow > 0)
                {
                    grid[currentColumn, currentRow].SouthDoor.SetActive(false);
                }
                if (currentRow < data.MapHeight - 1)
                {
                    grid[currentColumn, currentRow].NorthDoor.SetActive(false);
                }
            }
        }

        for (int i = 0; i < spawners.Count; i++)
        {
            spawners[i].SpawnEnemies(allWaypoints);
        }

        pm.SpawnPlayers();

    }



    public Room GetRandomTile() {
        return tilePrefabs[UnityEngine.Random.Range(0, tilePrefabs.Length)];
    }
}
