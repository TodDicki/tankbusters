﻿using UnityEngine;
using System.Collections;

public class MiniMapCameraFollow : MonoBehaviour {

    [SerializeField]
    private float YOffset = 50;
    [SerializeField]
    private Transform positionToFollow = null;

    private Transform tf = null;
	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        tf.position = positionToFollow.position + new Vector3(0, YOffset, 0);
	}
}
