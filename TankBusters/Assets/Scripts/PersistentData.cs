﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentData : MonoBehaviour {

    private float sfxVol = 1;
    private float mVol = 1;

    public bool MapOfTheDay
    {
        get;
        set;
    }

    public int MapWidth
    {
        get;
        set;
    }

    public int MapHeight
    {
        get;
        set;
    }

    public int Seed
    {
        get;
        set;
    }

    public float SFXVolume
    {
        get { return sfxVol; }
        set { sfxVol = value; }
    }

    public float MusicVolume
    {
        get { return mVol; }
        set { mVol = value; }
    }

    public int PlayerCount
    {
        get;
        set;
    }

    public int DifficultyLevel
    {
        get;
        set;
    }


    private void Awake()
    {
        if (FindObjectOfType<PersistentData>() != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

}
