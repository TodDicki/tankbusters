﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersistentDataManager : MonoBehaviour {

    [SerializeField]
    private Toggle mapOfDayToggle = null;
    [SerializeField]
    private InputField seedInputField = null;
    [SerializeField]
    private InputField widthInputField = null;
    [SerializeField]
    private InputField heightInputField = null;
    [SerializeField]
    private Slider playerSlider = null;
    [SerializeField]
    private Text playerNumDisplay = null;
    [SerializeField]
    private Slider sfxSlider = null;
    [SerializeField]
    private Slider musicSlider = null;
    [SerializeField]
    private Slider difficultySlider = null;

    PersistentData data = null;

    private void Start()
    {
        data = FindObjectOfType<PersistentData>();

        if (data != null)
        {
            //set the UI values to the persistent data's values
            if (mapOfDayToggle)
                mapOfDayToggle.isOn = data.MapOfTheDay;
            if (seedInputField)
                seedInputField.text = data.Seed.ToString();
            if (widthInputField)
                widthInputField.text = data.MapWidth.ToString();
            if (heightInputField)
                heightInputField.text = data.MapHeight.ToString();
            if (sfxSlider)
                sfxSlider.value = data.SFXVolume;
            if (musicSlider)
                musicSlider.value = data.MusicVolume;
            if (playerSlider)
                playerSlider.value = data.PlayerCount - 1;
            if (difficultySlider)
                difficultySlider.value = data.DifficultyLevel;


            //Set up UI events to affect the persistent data
            if (mapOfDayToggle)
                mapOfDayToggle.onValueChanged.AddListener(SetMapOfDay);
            if (seedInputField)
                seedInputField.onValueChanged.AddListener((str) =>
                {
                    SetSeed(Int32.Parse(str));
                });
            if (widthInputField)
                widthInputField.onValueChanged.AddListener((str) =>
                {
                    SetMapWidth(Int32.Parse(str));
                });
            if (heightInputField)
                heightInputField.onValueChanged.AddListener((str) =>
                {
                    SetMapHeight(Int32.Parse(str));
                });
            if (sfxSlider)
                sfxSlider.onValueChanged.AddListener((newVol) =>
                {
                    SetSFXVolume(newVol);
                });
            if (musicSlider)
                musicSlider.onValueChanged.AddListener((newVol) =>
                {
                    SetMusicVolume(newVol);
                });
            if (playerSlider)
                playerSlider.onValueChanged.AddListener((val) =>
                {
                    SetPlayerCount((int)val);
                });
            if (difficultySlider)
                difficultySlider.onValueChanged.AddListener((val) =>
                {
                    SetDifficulty((int)val);
                });

            //make sure that the values aren't something that would break the game if the user hit play without changing values
            if (widthInputField && Int32.Parse(widthInputField.text) == 0)
                widthInputField.text = "1";
            if (heightInputField && Int32.Parse(heightInputField.text) == 0)
                heightInputField.text = "1";
            if (playerSlider && playerSlider.value <= 0)
            {
                playerSlider.value = 0;
                SetPlayerCount(0);
            }
            if (difficultySlider && difficultySlider.value <= 1)
            {
                difficultySlider.value = 1;
                SetDifficulty(1);
            }
        }

    }

    private void SetMapOfDay(bool mod)
    {
        data.MapOfTheDay = mod;
    }

    private void SetSeed(int seed)
    {
        data.Seed = seed;
    }

    private void SetMapHeight(int height)
    {
        data.MapHeight = height;
    }

    private void SetMapWidth(int width)
    {
        data.MapWidth = width;
    }

    private void SetSFXVolume(float vol)
    {
        data.SFXVolume = vol;
    }

    private void SetMusicVolume(float vol)
    {
        data.MusicVolume = vol;
    }

    private void SetPlayerCount(int amount)
    {
        data.PlayerCount = amount + 1;
        if (playerNumDisplay != null)
            playerNumDisplay.text = (amount + 1).ToString();
    }

    private void SetDifficulty(int level)
    {
        data.DifficultyLevel = level;
    }
}
