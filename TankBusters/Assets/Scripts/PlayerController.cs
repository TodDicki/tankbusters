﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    private Transform tf = null;
    private Mover mover = null;
    private Shooter shooter = null;
    private ControlScheme controls = null;

    public ControlScheme Controls
    {
        set { controls = value; }
    }

    private void Start()
    {
        tf = GetComponent<Transform>();
        mover = GetComponent<Mover>();
        shooter = GetComponent<Shooter>();
    }

    // Update is called once per frame
    void Update () {
		Vector3 directionToMove = tf.forward * Input.GetAxis(controls.verticalAxis);
        float rotateDirection = Input.GetAxis(controls.horizontalAxis);//direction the player will rotate
                                             
		mover.Move (directionToMove);
		mover.Turn (rotateDirection);

		if (Input.GetButton (controls.fireInput)) {
			shooter.Fire ();
		}

	}
}
