﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    [SerializeField]
    private PlayerController playerPrefab = null;
    [SerializeField]
    private GameObject gameOverUI = null;
    [SerializeField]
    private ControlScheme[] playerControlSchemes;

    private PersistentData data = null;

    private List<Transform> playerSpawns = new List<Transform>();

    int remainingPlayers = 0;

    public List<Transform> PlayerSpawns
    {
        get { return playerSpawns; }
    }

    private void Awake()
    {
        data = FindObjectOfType<PersistentData>();
    }

    public void SpawnPlayers()
    {
        List<Camera> playerCams = new List<Camera>(data.PlayerCount);

        for (int i = 0; i < data.PlayerCount; i++)
        {
            int randIndex = Random.Range(0, playerSpawns.Count - 1);
            PlayerController pc = Instantiate(playerPrefab, playerSpawns[randIndex].position, Quaternion.identity);
            pc.Controls = playerControlSchemes[i];
            playerCams.Add(pc.GetComponentInChildren<Camera>());
            remainingPlayers++;

            pc.GetComponent<Health>().OnDeath.AddListener(() =>
            {
                remainingPlayers--;
                CheckDeaths();
            });
        }

        //I don't like doing this, as I feel like I can use a modulus trick to do it instead, but I couldn't figure it out.
        if (playerCams.Count == 1)
        {
            playerCams[0].rect = new Rect(0, 0, 1, 1);
        }
        else if (playerCams.Count == 2)
        {
            playerCams[0].rect = new Rect(0, 0.5f, 1, 0.5f);
            playerCams[1].rect = new Rect(0, 0, 1, 0.5f);
        }
        else
        {
            playerCams[0].rect = new Rect(0, 0.5f, 0.5f, 0.5f);
            playerCams[1].rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
            playerCams[2].rect = new Rect(0, 0, 0.5f, 0.5f);
            if (playerCams.Count == 4)
                playerCams[3].rect = new Rect(0.5f, 0, 0.5f, 0.5f);
        }
    }

    void CheckDeaths()
    {
        if (remainingPlayers <= 0)
            gameOverUI.SetActive(true);
    }
}
