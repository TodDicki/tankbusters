﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

public class Projectile : MonoBehaviour, ItemHeapItem {
	
    [SerializeField]
	private float destroyTimeDelay;//the projectiles destruction delay

    [SerializeField]
    private float damage = 1.0f;

    [SerializeField]
    private AudioClip impactSound = null;

    [SerializeField]
    private GameObject particlePrefab = null;

    private ItemHeap heap = null;

    private UnityEvent onRetrieval = new UnityEvent();
    private UnityEvent onReturn = new UnityEvent();

    public ItemHeap Heap
    {
        set
        {
            heap = value;
        }
    }

    public UnityEvent OnRetrieval
    {
        get
        {
            return onRetrieval;
        }
    }

    public UnityEvent OnReturn
    {
        get
        {
            return onReturn;
        }
    }

    private void Start()
    {
        AudioManager am = FindObjectOfType<AudioManager>();
        onRetrieval.AddListener(() => 
        {
            StartCoroutine(LifeTimer());
        });

        onReturn.AddListener(() => 
        {
            Instantiate(particlePrefab, transform.position, Quaternion.identity);
            am.PlayAudio(impactSound, transform.position);
        });
    }

    IEnumerator LifeTimer()
    {
        yield return new WaitForSeconds(destroyTimeDelay);
        if(gameObject.activeSelf)
            heap.ReturnObject(gameObject);
    }

	/// <summary>
	/// Raises the collision enter event.
	/// </summary>
	/// <param name="collider">Collider.</param>
	void OnCollisionEnter(Collision collider){
		if(collider.gameObject.GetComponent<Health>()){
			collider.gameObject.GetComponent<Health>().CurrentHealth -= damage;//decrements the health
        }
        heap.ReturnObject(gameObject);
	}
}
