﻿using UnityEngine;
using System.Collections;

public class Room : MonoBehaviour {
    [SerializeField]
	private GameObject doorNorth = null;
    [SerializeField]
	private GameObject doorSouth = null;
    [SerializeField]
	private GameObject doorEast = null;
    [SerializeField]
	private GameObject doorWest = null;

    public GameObject NorthDoor
    {
        get { return doorNorth; }
    }

    public GameObject SouthDoor
    {
        get { return doorSouth; }
    }

    public GameObject EastDoor
    {
        get { return doorEast; }
    }

    public GameObject WestDoor
    {
        get { return doorWest; }
    }
}
