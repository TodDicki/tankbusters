﻿using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour {

    [SerializeField]
    private float health = 3;
    [SerializeField]
    private AudioClip deathClip = null;
    [SerializeField]
    private GameObject[] spawnedOnDeath;

    public UnityEvent OnDeath = new UnityEvent();


    public float CurrentHealth
    {
        get { return health; }
        set
        {
            health = value;
            if (health <= 0)
            {
                
                OnDeath.Invoke();
                
            }
        }
    }

	// Use this for initialization
	void Start () {
        if (spawnedOnDeath.Length > 0)
        {
            OnDeath.AddListener(() =>
            {
                for (int i = 0; i < spawnedOnDeath.Length; i++)
                {
                    Instantiate(spawnedOnDeath[i], transform.position, Quaternion.identity);
                }
                
            });
        }
        AudioManager am = FindObjectOfType<AudioManager>();
        OnDeath.AddListener(() => am.PlayAudio(deathClip, transform.position));
        OnDeath.AddListener(() => { Destroy(gameObject); });
	}
}
