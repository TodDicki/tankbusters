﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour {
	
    //private CharacterController controller = null;
    private Transform tf = null;

    [SerializeField]
    private float forwardMovementSpeed = 3;//rate at which the tank can move forwards
    [SerializeField]
    private float backwardMovementSpeed = 2;//rate at which the tank can move backwards
    [SerializeField]
    private float turnSpeed = 50;//rate at which tank can turn


    // Use this for initialization
    void Awake () {
	    //controller = GetComponent<CharacterController>();
        tf = GetComponent<Transform>();
	}

	/// <summary>
	/// Move the specified direction.
	/// </summary>
	/// <param name="direction">Direction.</param>
	public void Move(Vector3 direction){//moves this object
        float speed = (direction.z > 0) ? forwardMovementSpeed : backwardMovementSpeed;
        tf.position += (direction * speed * Time.deltaTime);
        //controller.SimpleMove(direction * speed);
	}

	/// <summary>
	/// Turn the specified rotateDirection.
	/// </summary>
	/// <param name="rotateDirection">Rotate direction.</param>
	public void Turn(float rotateDirection){
		tf.Rotate (Vector3.up * Time.deltaTime * turnSpeed * rotateDirection);//rotates the game object
	}

	/// <summary>
	/// Rotates toward the targetPosition.
	/// </summary>
	/// <param name="targetPosition">Target position.</param>
	public void RotateTowards(Vector3 targetPosition)
	{
		Quaternion lookRotation = Quaternion.LookRotation (targetPosition - tf.position);//find rotation to look at
		tf.rotation = Quaternion.RotateTowards (tf.rotation, lookRotation, turnSpeed * Time.deltaTime);//rotate to look
	}
}
