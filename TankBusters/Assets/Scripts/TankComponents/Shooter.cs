﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {

    [SerializeField]
    private Transform projectileSpawn = null;
    [SerializeField]
    private ItemHeap projectileHeap = null;
    [SerializeField]
    private int bulletsToFire = 1;
    [SerializeField]
    private float fireRate = 2;//rate at which the projectile fires
    [SerializeField]
    private float projectileFireStrength = 300;

    private float nextFire;//time the next shot can be fired

	// Use this for initialization
	void Start () {
	}
		
	/// <summary>
	/// Fire a projectile.
	/// </summary>
	public void Fire()//fires a projectile
	{
		if (Time.time > nextFire) {//if the appropriate amount of time has passed to fire
			nextFire = Time.time + fireRate;//set the next time to fire
            for (int x = 0; x < bulletsToFire; x++)
            {
                GameObject tankShellClone = projectileHeap.GetNext();//create a clone
                tankShellClone.transform.position = projectileSpawn.position;
                tankShellClone.transform.rotation = projectileSpawn.rotation;
                tankShellClone.GetComponent<Rigidbody>().AddForce(projectileSpawn.forward * projectileFireStrength);//add force to the projectile
            }
		}
	}
}
