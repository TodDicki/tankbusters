﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedDestroyer : MonoBehaviour {

    [SerializeField]
    private float timer = 1.0f;

	// Use this for initialization
	void Start () {
        Destroy(gameObject, timer);
	}

}
