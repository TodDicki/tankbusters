﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VideoPlayer : MonoBehaviour {

    [SerializeField]
    private MovieTexture video = null;

	// Use this for initialization
	void Start () {
        GetComponent<RawImage>().texture = video as MovieTexture;
        video.loop = true;
        video.Play();
	}
	
}
